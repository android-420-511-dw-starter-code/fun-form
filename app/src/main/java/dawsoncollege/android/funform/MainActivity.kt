package dawsoncollege.android.funform

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import dawsoncollege.android.funform.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: FormQuestionsRecyclerAdapter
    private lateinit var formQuestions: MutableList<FormQuestion>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onStart() {
        super.onStart()

        // TODO need to load the list of answered questions from where they were saved...
        formQuestions = mutableListOf()

        adapter = FormQuestionsRecyclerAdapter(resources, formQuestions)
        binding.formQuestionList.adapter = adapter
        binding.formQuestionList.layoutManager = LinearLayoutManager(this)
    }

    private fun getSelectedFormChoice(): FormChoice? =
        when (binding.formChoices.checkedRadioButtonId) {
            R.id.form_choice_a -> FormChoice.A
            R.id.form_choice_b -> FormChoice.B
            R.id.form_choice_c -> FormChoice.C
            R.id.form_choice_d -> FormChoice.D
            else -> null
        }

    private fun showFormQuestion(formQuestionNumber: Int) {
        binding.formQuestion.text = getString(getFormQuestionStringId(formQuestionNumber)!!)
        binding.formChoiceA.text =
            getString(getFormChoiceStringId(formQuestionNumber, FormChoice.A)!!)
        binding.formChoiceB.text =
            getString(getFormChoiceStringId(formQuestionNumber, FormChoice.B)!!)
        binding.formChoiceC.text =
            getString(getFormChoiceStringId(formQuestionNumber, FormChoice.C)!!)
        binding.formChoiceD.text =
            getString(getFormChoiceStringId(formQuestionNumber, FormChoice.D)!!)
    }
}