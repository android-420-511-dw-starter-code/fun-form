package dawsoncollege.android.funform

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import dawsoncollege.android.funform.databinding.ActivitySettingsBinding

class SettingsActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySettingsBinding
    private lateinit var adapter: FormQuestionsRecyclerAdapter
    private lateinit var formQuestions: MutableList<FormQuestion>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // TODO need to load the list of answered questions from where they were saved...
        formQuestions = mutableListOf()

        adapter = FormQuestionsRecyclerAdapter(resources, formQuestions)
        binding.formQuestionList.adapter = adapter
        binding.formQuestionList.layoutManager = LinearLayoutManager(this)
    }
}