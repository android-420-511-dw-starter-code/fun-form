package dawsoncollege.android.funform

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import dawsoncollege.android.funform.databinding.FormQuestionBinding

class FormQuestionsRecyclerAdapter(
    private val resources: Resources,
    private val formQuestions: MutableList<FormQuestion>
) :
    RecyclerView.Adapter<FormQuestionsRecyclerAdapter.ViewHolder>() {

    class ViewHolder(val binding: FormQuestionBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Here we create the binding from scratch, for a new ViewHolder
        val binding =
            FormQuestionBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Here we don't need to create the binding, we just take it from ViewHolder
        val binding = holder.binding
        val (number, choice) = formQuestions[position]

        // TODO need to inject FormQuestion into the layout...
    }

    override fun getItemCount(): Int = formQuestions.size
}