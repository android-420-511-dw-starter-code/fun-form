package dawsoncollege.android.funform

/**
 * Gets the id of the string resource representing a question
 */
fun getFormQuestionStringId(formQuestionNumber: Int): Int? =
    when (formQuestionNumber) {
        1 -> R.string.form_question_1
        2 -> R.string.form_question_2
        3 -> R.string.form_question_3
        4 -> R.string.form_question_4
        5 -> R.string.form_question_5
        6 -> R.string.form_question_6
        7 -> R.string.form_question_7
        8 -> R.string.form_question_8
        else -> null
    }

enum class FormChoice {
    A, B, C, D
}

/**
 * Gets the id of the string resource representing a choice, for a given question.
 */
fun getFormChoiceStringId(formQuestionNumber: Int, formChoice: FormChoice): Int? =
    when (formQuestionNumber) {
        1 -> when (formChoice) {
            FormChoice.A -> R.string.form_question_1_choice_a
            FormChoice.B -> R.string.form_question_1_choice_b
            FormChoice.C -> R.string.form_question_1_choice_c
            FormChoice.D -> R.string.form_question_1_choice_d
        }
        2 -> when (formChoice) {
            FormChoice.A -> R.string.form_question_2_choice_a
            FormChoice.B -> R.string.form_question_2_choice_b
            FormChoice.C -> R.string.form_question_2_choice_c
            FormChoice.D -> R.string.form_question_2_choice_d
        }
        3 -> when (formChoice) {
            FormChoice.A -> R.string.form_question_3_choice_a
            FormChoice.B -> R.string.form_question_3_choice_b
            FormChoice.C -> R.string.form_question_3_choice_c
            FormChoice.D -> R.string.form_question_3_choice_d
        }
        4 -> when (formChoice) {
            FormChoice.A -> R.string.form_question_4_choice_a
            FormChoice.B -> R.string.form_question_4_choice_b
            FormChoice.C -> R.string.form_question_4_choice_c
            FormChoice.D -> R.string.form_question_4_choice_d
        }
        5 -> when (formChoice) {
            FormChoice.A -> R.string.form_question_5_choice_a
            FormChoice.B -> R.string.form_question_5_choice_b
            FormChoice.C -> R.string.form_question_5_choice_c
            FormChoice.D -> R.string.form_question_5_choice_d
        }
        6 -> when (formChoice) {
            FormChoice.A -> R.string.form_question_6_choice_a
            FormChoice.B -> R.string.form_question_6_choice_b
            FormChoice.C -> R.string.form_question_6_choice_c
            FormChoice.D -> R.string.form_question_6_choice_d
        }
        7 -> when (formChoice) {
            FormChoice.A -> R.string.form_question_7_choice_a
            FormChoice.B -> R.string.form_question_7_choice_b
            FormChoice.C -> R.string.form_question_7_choice_c
            FormChoice.D -> R.string.form_question_7_choice_d
        }
        8 -> when (formChoice) {
            FormChoice.A -> R.string.form_question_8_choice_a
            FormChoice.B -> R.string.form_question_8_choice_b
            FormChoice.C -> R.string.form_question_8_choice_c
            FormChoice.D -> R.string.form_question_8_choice_d
        }
        else -> null
    }

data class FormQuestion(
    val formQuestionNumber: Int,
    var formChoice: FormChoice?
)